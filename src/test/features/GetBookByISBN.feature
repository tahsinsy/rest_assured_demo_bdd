Feature: Validate a book by ISBN
  Scenario: user will call web service to get a book by its ISBN
	Given book contains isbn 0760351791
	When user look for the book by ISBN
	Then the status code is 200
	And response includes the following
	| totalItems 	 		| 1 					|
	| kind					| books#volumes			|
   And response includes the following in any order
	| items.volumeInfo.title 					| Classic Motorcycles	    |
	| items.volumeInfo.subtitle 				| The Art of Speed	        |
	| items.volumeInfo.publisher 				| Motorbooks International	|   
	| items.volumeInfo.pageCount 				| 224					    |

